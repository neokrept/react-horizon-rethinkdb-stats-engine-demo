﻿using System;
using System.Data;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RethinkDb.Driver;
using RethinkDb.Driver.Model;
using RethinkDb.Driver.Net;
using RethinkDb.Driver.Ast;
using System.Data.SQLite;

namespace taskRunner
{
	public class Vote
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string id			{ get; set; }

		public string agent 	{ get; set; }
		public string headers { get; set; }

		public string ip 			{ get; set; }
		public string gameID  { get; set; }
		public long		time 		{ get; set; }
		public short 	vote 		{ get; set; }
	}

	public class Tasks
	{
		private Timer timer;
		public static RethinkDB r = RethinkDB.R;
		public static Connection conn;
		public static SQLiteConnection sqConn;

		public Tasks(int seconds = 10 * 1001) {
			timer = new Timer(seconds);
			timer.Elapsed += new ElapsedEventHandler(doAggregate);
			conn = r.Connection().Hostname("127.0.0.1").Port(RethinkDBConstants.DefaultPort).Timeout(60).Connect();
			string sqlitePath = System.AppDomain.CurrentDomain.BaseDirectory + "/../../.db/stats.sqlite";
			Console.WriteLine(sqlitePath);

			SQLiteConnection.CreateFile(sqlitePath);
			sqConn = new SQLiteConnection("Data Source=" + sqlitePath + ";Version=3;");
			sqConn.Open();

			string sql = "CREATE TABLE votes(id VARCHAR(46), score INT);CREATE UNIQUE INDEX votes_idx ON votes(id);";

			SQLiteCommand command = new SQLiteCommand(sql, sqConn);
			command.ExecuteNonQuery();
		}

		private static void doAggregate(object source, ElapsedEventArgs e) {
			long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
			ticks /= 10000000;
			long lastPeriod = (long)Math.Floor(ticks / 10.0) * 10;
			Console.WriteLine("Task called at: {0}, lastPeriod: {1}", e.SignalTime, lastPeriod);

			Cursor<Vote> votes = r.Db("web_service_demo").Table("gameVotes").Filter(x => x["time"].Gt(lastPeriod)).RunCursor<Vote>(conn);
			HashSet<string> uniquePerPeriod = new HashSet<string>();

			List<Vote> list = votes.ToList<Vote>();
			List<Vote> filtered = new List<Vote>();
			long period;
			string hash;

			foreach (var item in list) {
				period = (long)Math.Floor(item.time / 10.0);
				hash = item.gameID + "|" + item.ip + "|" + period;

				Console.WriteLine(hash + " ID = " + item.id);
				if (!uniquePerPeriod.Contains(hash)) {
					filtered.Add(item);
					uniquePerPeriod.Add(hash);
				}
			}

			string sql;

			foreach (var item in filtered) {
				sql = "INSERT OR IGNORE INTO votes(id, score) VALUES('" + item.gameID + "', 0);UPDATE votes SET score=score + '1' WHERE id='" + item.gameID + "';";
				Console.WriteLine("Time = {0}, ID = {1}, SQL = {2}", item.time, item.id, sql);

				SQLiteCommand command = new SQLiteCommand(sql, sqConn);
				command.ExecuteNonQuery();
			}
		}

		public void run() {
			timer.Start();
		}

		public void stop() {
			timer.Stop();
		}
	}
}
