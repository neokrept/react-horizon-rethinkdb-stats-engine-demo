﻿using System;

namespace taskRunner
{
	class MainClass
	{
		public static void Main(string[] args) {
			Tasks task = new Tasks(10 * 1000);
			task.run();
			Console.ReadKey();
		}
	}
}
