global._$HOST = 'localhost'
global._      = require('lodash')
global.needle = require('needle')
global.utils  = require('./src/lib/utils')
global.Buffer = require('buffer').Buffer
global.__project = {
  name: 'web_service_demo',
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
