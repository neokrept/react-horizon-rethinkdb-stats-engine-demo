const TYPE = process.env['npm_config_type'] || 'memory'
require('./globals')
require('nodejs-dashboard')

const query        = require('querystring')
const path         = require('path')
const express      = require('express')
const webpack      = require('webpack')
const config       = require('./webpack.config.dev')
const cookieParser = require('cookie-parser')
const session      = require('express-session')
const userAgent    = require('express-useragent')
const bodyParser   = require('body-parser')

const horizon = require('@horizon/server')

const https   = require('https')
const fs      = require('fs')
const oauth20 = require('./lib/oauth/oauth20.js')(TYPE)
const model   = require('./lib/oauth/model/' + TYPE)


const app      = express()
const httpPort = 80
const sslPort  = 443
const compiler = webpack(config)

const loopbackServer = app.listen(httpPort, 'localhost', function(err) {
  if (err) {
    console.log(err)
    return
  }
  console.log(`devServer is running at http://localhost:${httpPort}/`)
})

const options = {
  project_name: __project.name,
  auto_create_collection: true,
  permissions: false,
  auto_create_index: true,
  path: '/horizon',
  auth: {
    token_secret: 'pasodkpaojfpoaksfpoaskf',
    allow_anonymous: true,
    allow_unauthenticated: true,
    // success_redirect: '/success',
    // failure_redirect: '/fail',
    // create_new_users: true,
    // new_user_group:   'users',
  }
}

const onHttpRequest = (req, res) => {
  res.writeHead(404)
  res.end('File not found.')
}

const publicServer = https.createServer({
  key:  fs.readFileSync('data/crypto/horizon-key.pem'),
  cert: fs.readFileSync('data/crypto/horizon-cert.pem'),
}, app)

publicServer.listen(sslPort)

global.horizonServer = horizon([publicServer, loopbackServer], options)

global.horizonR = horizon.r

horizonServer._reql_conn._ready_promise.then((conn) => {
  global.horizonConn = conn
  console.log('express-rethink.db connection established')
})

oauth20.renewRefreshToken = true

app.set('oauth2', oauth20)
app.use(userAgent.express())
app.enable('trust proxy')

// Middleware
app.use(cookieParser())
app.use(session({ secret: 'catstypingoaidhjsohd19p2854u1p9uh1pt9uhp9185701thlbnn ajkhgao82ypth', resave: false, saveUninitialized: false }))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

require('./src/api/games')(app)

app.use(express.static('dist'))

app.get(/^\/[a-zA-Z]*$/, function(req, res) {
  res.sendFile(path.join(__dirname, 'data/html/index.html'))
})

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))
