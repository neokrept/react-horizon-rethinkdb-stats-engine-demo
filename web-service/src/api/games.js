var geoip = require('geoip-lite')

module.exports = (app) => {
  /* QUIZ endpoints */

  app.post('/a/games/create', function(req, res) {
    const record = {
      name:  req.body.name || '*unknown*',
      votes: 0,
      time:  (Date.now() / 1000)|0
    }

    try {
      horizonR.db(__project.name).table('gamesList').insert(record).run(horizonConn.connection()).then((stat) => {
        res.json({ok: true})
      }, (err) => {
        res.json({ok: false, err})
      })
    } catch(err) {
      res.json({ok: false, err})
    }
  })

  app.post('/a/games/voteFor', function(req, res) {
    if (!req.body.gameID || !req.body.vote) {
      res.json({ok: false, err: 'Invalid api request!'})
      return
    }
    const gameID = req.body.gameID
    const vote   = parseInt(req.body.vote)
    const record = (vote > 0)?{ votes: horizonR.row('votes').add(1) }:{ votes: horizonR.row('votes').add(-1) }

    try {
      horizonR.db(__project.name).table('gamesList').get(gameID).update(record).run(horizonConn.connection()).then((stat) => {
        const ip = req.ip.split(':')[0]
        const voteRecord = {
          vote:    (vote > 0)?1:-1,
          ip:      (ip === '127.0.0.1'?'localhost':JSON.stringify(geoip.lookup(ip))),
          agent:   JSON.stringify(req.useragent),
          headers: JSON.stringify(req.headers),
          time:    (Date.now() / 1000)|0,
          gameID
        }
        return horizonR.db(__project.name).table('gameVotes').insert(voteRecord).run(horizonConn.connection()).then(() => {
          res.json({ok: true})
        })
      }, (err) => {
        res.json({ok: false, err})
      })
    } catch(err) {
      res.json({ok: false, err})
    }
  })

}
