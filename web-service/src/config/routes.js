import React from 'react';
import App from '../components/App';
import Home from '../components/Home';
import Games from '../components/modules/games/Main'
import NotFound from '../components/NotFound';
import { Route, IndexRoute } from 'react-router';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="home" component={Home} />
    <Route path="games" component={Games} />
    <Route path="*" component={NotFound} />
  </Route>
);
