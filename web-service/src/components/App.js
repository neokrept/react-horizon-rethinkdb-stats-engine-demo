import React from 'react';
import { Link } from 'react-router';

class App extends React.Component {
  render() {
    return (
      <div className="wrapper container">
        <span className="row">
          <h1>My app</h1>
        </span>
        <span className="row">
          <nav>
            <li><Link to="/games">Games</Link></li>
            <li><Link to="/notfound">Not Found</Link></li>
          </nav>
        </span>
        <span className="row">
          <div className="detail">
            {this.props.children}
          </div>
        </span>
      </div>
    )
  }
}

export default App;
