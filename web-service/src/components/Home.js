import React from 'react';

class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      disabled: true,
    }
  }

  componentDidMount() {
    // As soon as this component is mounted, enable the input
    this.setState({
      disabled: false,
    });
  }

  render() {

    return (
      <div className="welcome">
        <h2>[app name]</h2>
      </div>
    )
  }
}

Home.propTypes = {
}

Home.defaultProps = {
}

export default Home;
