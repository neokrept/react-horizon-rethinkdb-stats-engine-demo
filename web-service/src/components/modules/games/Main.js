import React from 'react';


class Games extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      disabled:    true,
      gamesList:   [],
      gameVotes:   [],
      newGameName: ''
    }
  }

  subscribe() {
    console.log('here')
    this.props.gamesList.order("votes", "descending")
      .watch()
      .subscribe(gamesList => {
        this.setState({ gamesList })
      })

    this.props.gameVotes.order("time", "descending")
      .limit(8)
      .watch()
      .subscribe(gameVotes => {
        this.setState({ gameVotes })
      })
  }

  componentDidMount() {
    // As soon as this component is mounted, enable the input
    this.setState({
      disabled: false
    });

    // Initiate the changefeeds
    console.log('here too')
    this.subscribe()
    console.log('after')
  }

  voteGame(pro, gameID, event) {
    pro = (pro)?true:false

    const vote = pro?1:-1
    const gamesList = _.reduce(this.state.gamesList, (o, item, index) => {
      if (item.id === gameID) {
        item.votes = ((item.votes)?item.votes:0) + vote
        o.push(item)
      } else {
        o.push(item)
      }
      return o
    }, [])
    const headers = { 'content-type': 'application/json;charset=UTF-8' }

    request.post({ url: 'http://localhost/a/games/voteFor', headers, form: { gameID, vote }}, function(err, res, body) {
      console.log(err, body)
    })
  }

  createGame(event) {
    const name = this.state.newGameName
    const headers = { 'content-type': 'application/json;charset=UTF-8' }
    request.post({ url: 'http://localhost/a/games/create', headers, form: { name }}, function(err, res, body) {
      console.log(err, body)
    })
  }

  updateNewGameName(event) {
    const name = event.target.value
    this.setState({ newGameName: name })
  }

  render() {
    const gamesList = this.state.gamesList.map((item) => (
      <div className='games-list-item row' key={item.id}>
        <span className='games-list-name seven columns'>{item.name}</span>
        <span className='games-list-votes one column'>{item.votes}</span>
        <span className='games-list-button two columns' onClick={this.voteGame.bind(this, true, item.id)}>
          <i className='fa fa-plus'></i>
        </span>
        <span className='games-list-button two columns' onClick={this.voteGame.bind(this, false, item.id)}>
          <i className='fa fa-minus'></i>
        </span>
      </div>
    ), this)

    return (
      <span className='games row'>
        <div className='games-new container'>
          <span className='games-new-title row'>
            <span className='twelve columns'>{this.state.newGameName}</span>
          </span>
          <span className='games-new-input row'>
            <span className='twelve columns'>
              <input type='text' placeholder='Diablo II' onChange={this.updateNewGameName.bind(this)} />
              <button onClick={this.createGame.bind(this)}><i className='fa fa-plus'></i>Add game to the list</button>
            </span>
          </span>
        </div>
        <div className='games-list container'>
          {gamesList}
        </div>
      </span>
    )
  }
};

Games.propTypes = {
  gamesList: React.PropTypes.object.isRequired,
  gameVotes: React.PropTypes.object.isRequired,
};

Games.defaultProps = {
  gamesList: horizon("gamesList"),
  gameVotes: horizon("gameVotes"),
};

export default Games;
