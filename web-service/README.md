## Getting started
1. Clone this repo
2. Run ```npm install```

## Start dev
1. Run ```npm run start``` command
2. Open browser at [http://localhost/](http://localhost/)

# OR #
1. Run ```npm run debug``` for a nodejs-dashboard dashboard ^^

## Build bundle
1. Run ```npm run build``` command
2. Your compiled app file is located at dist folder

## Contributing
Feel free to open issue or post pull request

All contributions are appreciated!

## License
MIT License - Created by [neokrept]
